using UnityEditor;
using UnityEngine;

namespace VeryRare.DialogueSystem
{
    [CustomEditor(typeof(DialogueObject))]
    public class DialogueObjectInspector : Editor
    {
        bool showNodes;
        public override void OnInspectorGUI()
        {
            // serializedObject.Update();
            var sc = target as DialogueObject;
            if (GUILayout.Button("Edit Dialogue", GUILayout.Height(40)) )
            {
                XNodeEditor.NodeEditorWindow.Open(sc);
            }
            if (GUILayout.Button("Find StartNode"))
            {
                sc.FindStartNode();
            }

            // showNodes = GUILayout.Toggle(showNodes, "Show Nodes");
            // if ( showNodes )
            // {
            //     EditorGUILayout.PropertyField(serializedObject.FindProperty("startNode"));
            //     EditorGUILayout.PropertyField(serializedObject.FindProperty("nodes"));
            // }
            
            // EditorGUILayout.PropertyField(serializedObject.FindProperty("properties"));

            // serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}
