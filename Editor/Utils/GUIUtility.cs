using System;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{
    public static class GUIUtility
    {
        public static string TextInput(string text, string placeholder, bool area = false, float width = 0, float minHeight = 0, GUIStyle style = null) 
        {
            var newText =
            ( style == null) ? (
             area ? EditorGUILayout.TextArea(text, GUILayout.Width(width), GUILayout.MinHeight(minHeight)) : EditorGUILayout.TextField(text)
             ) :
             (area ? EditorGUILayout.TextArea(text, style, GUILayout.Width(width), GUILayout.MinHeight(minHeight)) : EditorGUILayout.TextField(text, style) );

            if (String.IsNullOrWhiteSpace(text)) 
            {
                const int textMargin = 2;
                var guiColor = GUI.color;
                GUI.color = Color.grey;
                var textRect = GUILayoutUtility.GetLastRect();
                var position = new Rect(textRect.x + textMargin, textRect.y, textRect.width + 30f, textRect.height);
                var w = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = 60f;
                GUI.Label(position, placeholder);
                EditorGUIUtility.labelWidth = w;
                GUI.color = guiColor;
            }
            return newText;
        }

        
        public static void CustomInputPortField( XNode.NodePort port, Color col, GUIContent label, params GUILayoutOption[] options) {
            if (port == null) return;
            EditorGUILayout.LabelField(label, options);
       
            Vector2 position = GUILayoutUtility.GetLastRect().position - new Vector2(16, 0);
            Rect rect = new Rect(position, new Vector2(16, 16));

            NodeEditorGUILayout.DrawPortHandle(rect, col, col);

            // Register the handle position
            Vector2 portPos = rect.center;
            NodeEditor.portPositions[port] = portPos;
        }
        
    }
}