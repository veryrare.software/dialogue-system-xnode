using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Reflection;
using System.Text.RegularExpressions;

[CustomPropertyDrawer(typeof(NestableButtonAttribute))]
public class NestableButtonDrawer : NestablePropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
	{
		Initialize(prop, true);
		var a = attribute as NestableButtonAttribute;
		var p1 = position;
		p1.width = position.width - 70;
		var p2 = position;
		p2.x = p1.x + p1.width;
		p2.width = 70;
		EditorGUI.PropertyField(p1, prop, label);
		if (GUI.Button(p2, a.methodName ))
		{
			var method = objectType.GetMethod(a.methodName);
			method.Invoke(propertyObject, new object []{});
		}
	}
}