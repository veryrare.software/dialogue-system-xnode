using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{
    [CustomEditor(typeof(DialogueRunner))]
    public class DialogueRunnerEditor :Editor
    {
        private List<DialogueObject> objs;
        private string [] names;

        void OnEnable()
        {
            objs = FindAssetsByType<DialogueObject>();
            names = objs.ConvertAll(x=>x.name).ToArray();
        }

        public override void OnInspectorGUI()
        {
            var sc = target as DialogueRunner;
            if (GUILayout.Button("Open Graph"))
            {
                NodeEditorWindow.Open(sc.dialogueObject);
            }

            if (GUILayout.Button("Update Lines"))
                sc.UpdateLines();

            if (sc.dialogueObject == null && GUILayout.Button("Create new Dialogue"))
            {
                var lastPath = EditorPrefs.HasKey("DialogueObjectLastPath") ?  EditorPrefs.GetString("DialogueObjectLastPath") : null;
                var path = string.IsNullOrEmpty(lastPath) ?
                    EditorUtility.SaveFilePanelInProject("Create new dialogue", "Dialogue", "asset", "")
                    : EditorUtility.SaveFilePanelInProject("Create new dialogue", "Dialogue", "asset", "", lastPath);
                EditorPrefs.SetString("DialogueObjectLastPath", path);
                var newObject = ScriptableObject.CreateInstance<DialogueObject>();
                AssetDatabase.CreateAsset(newObject, path);
                sc.dialogueObject = newObject;
                NodeEditorWindow.Open(sc.dialogueObject);
            }

            if ( !Application.isPlaying)
            {
                serializedObject.Update();
                var objIdx = objs.IndexOf(sc.dialogueObject);
                objIdx = EditorGUILayout.Popup("Select Dialogue Object:", objIdx, names);
                // if ( objIdx >= 0 && objIdx < objs.Count)
                //     sc.dialogueObject = objs[objIdx];    
                serializedObject.ApplyModifiedProperties();
            }
            
            if (!Application.isPlaying)
                sc.SyncBinding();
            base.OnInspectorGUI();
        }

        public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
            List<T> assets = new List<T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for( int i = 0; i < guids.Length; i++ )
            {
                string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
                T asset = AssetDatabase.LoadAssetAtPath<T>( assetPath );
                if( asset != null )
                {
                    assets.Add(asset);
                }
            }
            return assets;
        }
    }
}
