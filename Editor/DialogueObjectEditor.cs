using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{
    [CustomNodeGraphEditor(typeof(DialogueObject), "DialogueObject.Settings")]
    public class DialogueObjectEditor : NodeGraphEditor 
    {
        private DialogueObject graph;


        public override void OnDropObjects(UnityEngine.Object[] objects)
        {
            Debug.Log("DialogueObjectEditor -> OnDropObjects TBA");
        }

        public override string GetNodeMenuName(Type type) 
        {
            if (typeof(BaseNode).IsAssignableFrom(type)) 
            {
                var s = type.ToString().Split('.');
                return s[s.Length-1];
            } 
            else 
                return null;
        }

        [System.Serializable]
        public class CustomSettings : NodeEditorPreferences.Settings
        {
            public bool additionalLineSettings;
        } 

        public override NodeEditorPreferences.Settings GetDefaultPreferences() 
        {
            return new CustomSettings() 
            {
                maxZoom = 2f,
                minZoom = 0.7f,
                gridSnap = true,
                
                gridBgColor =   new Color(0.1254902f,0.1254902f,0.1254902f,1f),
                gridLineColor = new Color(0.1254902f,0.1254902f,0.1254902f,1f),
                typeColors = new Dictionary<string, Color>() 
                {
                    { typeof(BaseNode).PrettyName(),        new Color(1,1,1) },
                    { typeof(DialogueNode).PrettyName(),    new Color(1,1,1) },
                    { typeof(TriggerNode).PrettyName(),     new Color(1, 1f,0.3f) },
                    { typeof(DialogueNPCNode).PrettyName(), new Color(1,1,1) },
                    { typeof(LogicNode).PrettyName(),       new Color(0.3f,0.7f,1) },
                    { typeof(Actor).PrettyName(),           new Color(0.85f, 0.4f, 0.3f) },
                    { typeof(bool).PrettyName(),            new Color(0.25f, 0.45f, 0.7f) },
                    
                },
            };
        }

        private static GUIStyle style;
        private static Texture2D bgTex;
        public static GUIStyle getBgStyle()
        {
            if ( style == null) style = new GUIStyle();
            if ( bgTex == null ) 
            {
                bgTex = new Texture2D(1, 1);
                bgTex.SetPixel(0, 0, new Color(0.1f,0.1f,0.1f,0.9f));
                bgTex.Apply();
            }
            style.normal.background = bgTex;
            return style;
        }

        bool showProps;
        public override void OnGUI()
        {
            base.OnGUI();
            Init();

            if ( graph.startNode == null || graph.startNode.GetInputValue<DialogueNode>("input") != null)
                graph.FindStartNode();

            GUILayout.BeginVertical(getBgStyle(), GUILayout.Width(200));
            

            showProps = GUILayout.Toggle(showProps, "Show props (" + graph.name +")");
            if (showProps)
            {
                EditorGUILayout.Popup("Edit Language:", 0, graph.langs);
                if ( GUILayout.Button("Select Dialogue Object") )
                {
                    Selection.activeObject = graph;
                }
                if ( GUILayout.Button("Import Ink File") )
                {

                }
                GUILayout.BeginHorizontal();
                if ( GUILayout.Button("Import JSON File") )
                {

                }
                if ( GUILayout.Button("Export JSON File") )
                {
                    var path = EditorUtility.SaveFilePanel("Export Json File", Application.dataPath, graph.name, "json" );
                    if ( !string.IsNullOrEmpty(path) )
                        File.WriteAllText(path, graph.SerializeToJson());
                }
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if ( GUILayout.Button("Export CSV File") )
                {

                }
                if ( GUILayout.Button("Import CSV File") )
                {
                    
                }
                GUILayout.EndHorizontal();
                // if ( GUILayout.Button("Add Property") )
                // {
                // }
                AbstractProperty propToDelete = null;
                foreach(var p in graph.properties)
                {
                    GUILayout.BeginHorizontal();
                    var n = p.GetType().PrettyName();
                    n = n.Substring(n.LastIndexOf(".")+1);
                    n = n.Replace("Property","");
                    GUILayout.Label(p.name + " (" + n + ")", GUILayout.Width(150));
                    if ( p is StringProperty s)
                        GUILayout.TextField(s.value, GUILayout.Width(50));
                    else if ( p is BoolProperty b)
                        GUILayout.Toggle(b.value, "", GUILayout.Width(50));
                    else if ( p is IntProperty i)
                        EditorGUILayout.IntField(i.value, GUILayout.Width(50));
                    else if ( p is ActorProperty a)
                        EditorGUILayout.ObjectField(a.value, typeof(Actor), true, GUILayout.Width(50));
                    else if ( p is TriggerProperty t)
                        GUILayout.Label("", GUILayout.Width(50));

                    if ( GUILayout.Button("Select", GUILayout.Width(50)) )
                    {
                        Selection.activeObject = p;
                    }
                    if ( GUILayout.Button("Rename", GUILayout.Width(50)) )
                    {
                        CustomRenamePopup.Show(p, graph);
                    }
                    if ( GUILayout.Button("X", GUILayout.Width(25)) )
                    {
                        propToDelete = p;
                    }

                    GUILayout.EndHorizontal();
                }
                if ( propToDelete != null)
                {
                    graph.properties.Remove(propToDelete);
                    var path = AssetDatabase.GetAssetPath(propToDelete);
                    AssetDatabase.RemoveObjectFromAsset(propToDelete);
                }
            }

            GUILayout.EndVertical();
        }


         [SettingsProvider]
        public static SettingsProvider CreateXNodeSettingsProvider() {
            SettingsProvider provider = new SettingsProvider("Preferences/Node Editor Extra", SettingsScope.User) {
                guiHandler = (searchContext) => { CustomPreferencesGUI(); },
                keywords = new HashSet<string>(new [] { "xNode", "node", "editor", "graph", "connections", "noodles", "ports" , "dialogue" })
            };
            return provider;
        }

        private static void CustomPreferencesGUI()
        {
            EditorGUILayout.LabelField("Custom Dialogue preferences");
            // todo which properties of conversation line should be visible
            // todo what default languages are visible
            //
        }

        void Init()
        {
            if (graph == null)
            {
                graph = target as DialogueObject;
            } 
        }

    }

}