using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{
    [NodeEditor.CustomNodeEditor(typeof(BoolNode))]
    public class BoolNodeEditor : AbstractPropertyNodeEditor<BoolProperty>
    {
    }
}