using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(AndNode))]
    public class AndNodeEditor : NodeEditor
    {
        AndNode n;

        void Init()
        {
            if (n == null)
            {
                n = target as AndNode;
            } 
        }
        
        public override void OnBodyGUI() 
        {
            Init();
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("inputA"), GUILayout.Width(30));
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("inputB"), GUILayout.Width(30));
            GUILayout.EndVertical();
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("output"), GUILayout.Width(30));
            GUILayout.EndHorizontal();
        }
    }

}