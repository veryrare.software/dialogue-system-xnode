using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(IntNode))]
    public class IntNodeEditor : AbstractPropertyNodeEditor<IntProperty>
    {
        public override void InnerBodyGUI()
        {
            base.InnerBodyGUI();
            GUILayout.BeginHorizontal();
            var sc = target as IntNode;
            sc.comparison = (IntNode.Comparison) EditorGUILayout.EnumPopup(sc.comparison);
            sc.compareToInt = EditorGUILayout.IntField(sc.compareToInt, GUILayout.Width(40));
            GUILayout.EndHorizontal();
        }
    }




}
