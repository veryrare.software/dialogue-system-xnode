using System;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;
using XNodeEditor.Internal;
using static XNode.Node;

namespace VeryRare.DialogueSystem
{

    [CustomNodeEditor(typeof(DialogueSelfNode))]
    public class DialogueNodeEditor : NodeEditor 
    {
        private DialogueSelfNode n;
        private DialogueObject graph;
        private GUIStyle messageStyle;

        public override void AddContextMenuItems(GenericMenu menu)
        {
            base.AddContextMenuItems(menu);
            menu.AddItem(new GUIContent("Toggle Start Node"), false, () => graph.startNode = graph.startNode == n ? null : n );
        }

        public override void OnHeaderGUI()
        {
            Init();

            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Space(5);
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetInputPort("input"), GUILayout.Width(50));
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            base.OnHeaderGUI();
            GUILayout.FlexibleSpace();
            GUILayout.Label(graph.startNode == n ? "->" : "", GUILayout.Width(20),GUILayout.Height(27));
            GUILayout.Label(NodeEditorUtilities.NodeDefaultName(n.GetType()).Replace("Dialogue", ""), GUILayout.Width(30),GUILayout.Height(27));
            GUILayout.EndHorizontal();
        }

        public override void OnBodyGUI() 
        {
            Init();
            serializedObject.Update();

            var c = GUI.backgroundColor;
            GUI.backgroundColor = new Color(0,0,0,0.2f);
            GUILayout.BeginHorizontal();
            if ( n.HasPort("actor") )
                NodeEditorGUILayout.PortField(new GUIContent(""), n.GetInputPort("actor"), GUILayout.Width(30));

            GUILayout.Label("Limit", GUILayout.Width(40));
            n.timeLimit = EditorGUILayout.FloatField("", n.timeLimit, GUILayout.Width(30));
            GUILayout.Space(30);
            GUILayout.Label("Delay", GUILayout.Width(40));
            n.timeDelay = EditorGUILayout.FloatField("", n.timeDelay, GUILayout.Width(30));
            GUILayout.EndHorizontal();
            GUI.backgroundColor = c;

            int removeIdx = -1;
            for(int i = 0; i < n.lines.Count; i++)
            {
                var l = n.lines[i];
                GUILayout.Space(20);
                GUILayout.BeginHorizontal();
                {
                    if ( n is DialogueNPCNode )
                    {
                        GUILayout.Space(30);
                    } else 
                    {
                        Color col1 = NodeEditorWindow.current.graphEditor.GetPortColor(l.HiddenPort) * 0.7f;
                        GUIUtility.CustomInputPortField(l.HiddenPort, col1, new GUIContent(""), GUILayout.Width(30));
                    }

                    l.message = GUIUtility.TextInput(l.message, "message", true, 342, 20);
                    GUILayout.FlexibleSpace();
                    NodeEditorGUILayout.PortField(GUIContent.none, l.OutputPort, GUILayout.Width(30));
                }
                GUILayout.EndHorizontal();

                // 2. line
                GUILayout.BeginHorizontal();

                Color col2 = NodeEditorWindow.current.graphEditor.GetPortColor(l.CheckPort);
                GUIUtility.CustomInputPortField(l.CheckPort, col2, new GUIContent(""), GUILayout.Width(30));

                if ( GUILayout.Button("X", GUILayout.Width(20)) ) removeIdx = i;

                // todo hide these fields if disabled in custom settings
                l.clip = EditorGUILayout.ObjectField(l.clip, typeof(AudioClip), false) as AudioClip;
                var labelStyle = new GUIStyle(EditorStyles.label);
                labelStyle.CalcMinMaxWidth(new GUIContent("anim"), out var minWidth, out var maxWidth);
                labelStyle.fixedWidth = minWidth;
                labelStyle.stretchWidth = false;
                l.anim = GUIUtility.TextInput(l.anim, "anim");
                l.mood = GUIUtility.TextInput(l.mood, "mood");


                l.textAlign = (TextAlign) EditorGUILayout.EnumPopup(l.textAlign, GUILayout.Width(70));

                GUILayout.FlexibleSpace();
                NodeEditorGUILayout.PortField(GUIContent.none, l.TriggerPort, GUILayout.Width(30));

                GUILayout.EndHorizontal();

            }
            // remove node if any marked
            if ( removeIdx != -1)  RemoveLine(removeIdx);
            if ( GUILayout.Button("Add Line") ) AddNewLine();

            serializedObject.ApplyModifiedProperties();
        }

        void AddNewLine()
        {
            var o = n.AddDynamicOutput(typeof(DialogueNode),ConnectionType.Override, TypeConstraint.None, Guid.NewGuid().ToString());
            var c = n.AddDynamicInput(typeof(bool),         ConnectionType.Override, TypeConstraint.None, Guid.NewGuid().ToString());
            var h = n.AddDynamicInput(typeof(bool),         ConnectionType.Override, TypeConstraint.None, Guid.NewGuid().ToString());
            var t = n.AddDynamicOutput(typeof(TriggerNode), ConnectionType.Multiple, TypeConstraint.Inherited, Guid.NewGuid().ToString());
            var l = new ConversationLine(n, o.fieldName, c.fieldName, t.fieldName, h.fieldName);
            n.lines.Add(l);
        }

        void RemoveLine(int idx)
        {
            var l = n.lines[idx];
            n.lines.RemoveAt(idx);
            n.RemoveDynamicPort(l.OutputPort);
            n.RemoveDynamicPort(l.CheckPort);
            n.RemoveDynamicPort(l.TriggerPort);
            n.RemoveDynamicPort(l.HiddenPort);
        }

        void Init()
        {
            if (n == null) 
            {
                n = target as DialogueSelfNode;
                graph = n.graph as DialogueObject;
                if ( n.lines.Count == 0) AddNewLine();
                messageStyle = new GUIStyle(EditorStyles.textArea);
                messageStyle.fontSize = (int)(messageStyle.fontSize * 0.9f);
            }
        }

    }

}
