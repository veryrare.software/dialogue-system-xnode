using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{
    public class AbstractPropertyNodeEditor<T> : NodeEditor where T : AbstractProperty
    {
        protected BaseNode baseNode;
        protected IPropertyNode<T> propertyNode;
        protected DialogueObject graph;
        protected string newPropName;
        protected bool creatingProp;

        void Init()
        {
            if (graph == null)
            {
                
                baseNode = target as BaseNode;
                graph = baseNode.graph as DialogueObject;
                propertyNode = (IPropertyNode<T>)target;
            } 
        }
        
        public override void AddContextMenuItems(GenericMenu menu)
        {
            base.AddContextMenuItems(menu);
            menu.AddItem(new GUIContent("Select Property"), false, ()=> Selection.activeObject = propertyNode.GetProperty());
            menu.AddItem(new GUIContent("Rename Property"), false, () => CustomRenamePopup.Show(propertyNode.GetProperty(), graph ));
            menu.AddItem(new GUIContent("Delete Property"), false, ()=>  graph.DeleteProperty(propertyNode.GetProperty()));
        }

        public override void OnBodyGUI() 
        {
            Init();
            
            GUILayout.BeginHorizontal();
            if ( baseNode.HasPort("input") )
                NodeEditorGUILayout.PortField(new GUIContent(""), baseNode.GetPort("input"), GUILayout.Width(30));

            GUILayout.BeginVertical();
            InnerBodyGUI();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            if ( baseNode.HasPort("output") )
                NodeEditorGUILayout.PortField(new GUIContent(""), baseNode.GetPort("output"), GUILayout.Width(30));

            GUILayout.EndHorizontal();
        }


        public virtual void InnerBodyGUI()
        {
            GUILayout.BeginHorizontal();

            if ( creatingProp )
            {
                newPropName = GUIUtility.TextInput(newPropName, "new prop");
                if (GUILayout.Button("X")) creatingProp = false;
                if (GUILayout.Button("✓"))
                {
                    var p  = graph.CreateProperty<T>(newPropName);
                    if ( p != null)
                    {
                        propertyNode.SetProperty(p);
                        creatingProp = false;
                    }
                } 
            } else 
            {
                if (  graph.properties == null || graph.properties.Count == 0) 
                {
                    creatingProp = true;
                } else {
                    var props = graph.properties.Where(x=>x.GetType() == typeof(T)).ToList();
                    var idx = props.IndexOf( propertyNode.GetProperty() );
                    var propNames = props.ConvertAll(x=>x.name).ToArray();
                    idx = EditorGUILayout.Popup(idx, propNames);
                    if (idx >= 0 && idx < props.Count)
                        propertyNode.SetProperty(props[idx] as T);
                    if (GUILayout.Button( "+")) creatingProp = true;
                }
            }

            GUILayout.EndHorizontal();
            GUILayout.Label( propertyNode.GetProperty() != null ? propertyNode.GetProperty().ToString() : null);
        }

    }
}