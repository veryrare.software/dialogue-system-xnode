using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(TriggerNode))]
    public class TriggerNodeEditor : AbstractPropertyNodeEditor<TriggerProperty>
    {
    }

}
