using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(NotNode))]
    public class NotNodeEditor : NodeEditor
    {
        NotNode n;

        void Init()
        {
            if (n == null)
            {
                n = target as NotNode;
            } 
        }
        
        public override void OnBodyGUI() 
        {
            Init();
            GUILayout.BeginHorizontal();
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("input"), GUILayout.Width(30));
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("output"), GUILayout.Width(30));
            GUILayout.EndHorizontal();
        }
    }

}