using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(LineVisitedNode))]
    public class LineVisitedNodeEditor : NodeEditor
    {
        LineVisitedNode n;
        DialogueObject graph;

        void Init()
        {
            if (n == null)
            {
                n = target as LineVisitedNode;
                graph = n.graph as DialogueObject;
            } 
        }
        
        public override void OnBodyGUI() 
        {
            Init();
            GUILayout.BeginHorizontal();
            var dialogueNodes = graph.nodes.Where(x=>x is DialogueSelfNode).ToList();
            var idx = dialogueNodes.IndexOf(n.dialogueNode);
            var names = dialogueNodes.Select(x=>x.name).ToArray();
            idx = EditorGUILayout.Popup(idx, names);
            if (idx >= 0 && idx < dialogueNodes.Count)
                n.dialogueNode = dialogueNodes[idx] as DialogueSelfNode;

            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("output"), GUILayout.Width(30));
            GUILayout.EndHorizontal();

            n.line = EditorGUILayout.IntField("line", n.line, GUILayout.Width(134));
        }
    }

}