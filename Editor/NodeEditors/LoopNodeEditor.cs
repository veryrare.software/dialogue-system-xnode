using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(LoopNode))]
    public class LoopNodeEditor : NodeEditor
    {
        LoopNode n;
        DialogueObject graph;

        void Init()
        {
            if (n == null)
            {
                n = target as LoopNode;
                graph = n.graph as DialogueObject;
            } 
        }
        
        public override void OnBodyGUI() 
        {
            Init();
            GUILayout.BeginHorizontal();
            NodeEditorGUILayout.PortField(new GUIContent(""), n.GetPort("input"), GUILayout.Width(30));
            var dialogueNodes = graph.nodes.Where(x=>x is DialogueSelfNode).ToList();
            var idx = dialogueNodes.IndexOf(n.dialogueNode);
            var names = dialogueNodes.Select(x=>x.name).ToArray();
            idx = EditorGUILayout.Popup(idx, names);
            if (idx >= 0 && idx < dialogueNodes.Count)
                n.dialogueNode = dialogueNodes[idx] as DialogueSelfNode;
            GUILayout.EndHorizontal();
        }
    }

}