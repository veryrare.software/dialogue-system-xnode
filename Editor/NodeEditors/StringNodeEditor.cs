using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(StringNode))]
    public class StringNodeEditor : AbstractPropertyNodeEditor<StringProperty>
    {
        public override void InnerBodyGUI()
        {
            base.InnerBodyGUI();
            var sc = target as StringNode;
            sc.compareToString = GUIUtility.TextInput(sc.compareToString, "str to comp");
        }
    }
}
