using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace VeryRare.DialogueSystem
{

    [NodeEditor.CustomNodeEditor(typeof(ActorNode))]
    public class ActorNodeEditor : AbstractPropertyNodeEditor<ActorProperty>
    {
    }

}
