
namespace VeryRare.ObjectiveSystem {

#if UNITY_EDITOR
    using System;
    using UnityEditor;
    using XNodeEditor;

[CustomNodeGraphEditor(typeof(ObjectiveGraph))]
public class ObjectiveGraphEditor : NodeGraphEditor 
{

    public override string GetNodeMenuName(Type type) 
    {
        if (typeof(LogicNode).IsAssignableFrom(type)) 
            return base.GetNodeMenuName(type).Replace("Very Rare/Objective System/", "Logic/");

            
        if (typeof(MathNode).IsAssignableFrom(type)) 
            return base.GetNodeMenuName(type).Replace("Very Rare/Objective System/", "Math/");

        if (typeof(ObjectiveGraphBaseNode).IsAssignableFrom(type)) 
            return base.GetNodeMenuName(type).Replace("Very Rare/Objective System/", "");
        
        return null;
    }

        /// <summary> Creates a copy of the original node in the graph </summary>
    public override XNode.Node CopyNode(XNode.Node original) 
    {
        Undo.RecordObject(target, "Duplicate Node");
        XNode.Node node = target.CopyNode(original);
        Undo.RegisterCreatedObjectUndo(node, "Duplicate Node");
        node.name = original.name;
        // AssetDatabase.AddObjectToAsset(node, target);
        if (NodeEditorPreferences.GetSettings().autoSave) AssetDatabase.SaveAssets();
        return node;
    }

}
#endif

}