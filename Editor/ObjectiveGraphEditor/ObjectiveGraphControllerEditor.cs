namespace VeryRare.ObjectiveSystem
{
    using System.Linq;

#if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine;
    using XNode;
    using XNodeEditor;

    [CustomEditor(typeof(ObjectiveGraphController), true)]
    public class ObjectiveGraphControllerEditor : SceneGraphEditor
    {
        ObjectiveGraph graphToClone;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            GUILayout.BeginHorizontal();
            // graphToClone = (ObjectiveGraph) EditorGUILayout.ObjectField("Graph to Clone", graphToClone, typeof(ObjectiveGraph), false);
            // if (GUILayout.Button("Clone Graph") && graphToClone != null) CloneGraph(graphToClone);
            GUILayout.EndHorizontal();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("objectiveNodes"));
            // EditorGUILayout.PropertyField(serializedObject.FindProperty("objectives"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onAddObjective"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onRemoveObjective"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onActivateObjective"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onCompleteObjective"));
            serializedObject.ApplyModifiedProperties();
        }

        public void CloneGraph(NodeGraph graph) 
        {
            var sceneGraph = target as SceneGraph;
            Undo.RecordObject(sceneGraph, "Clone graph");
            sceneGraph.graph = Instantiate(graph);
            sceneGraph.graph.name = sceneGraph.name + "-graph";
        }
    }


[CustomPropertyDrawer(typeof(ObjectiveNodeAttribute))]
public class ObjectiveNodeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        ObjectiveNodeAttribute a = attribute as ObjectiveNodeAttribute;

        if (property.propertyType == SerializedPropertyType.ObjectReference)
        {
            Component c = property.serializedObject.targetObject as Component;
            var conditions = c.GetComponent<ObjectiveGraphController>().GetObjectiveNodes();
            var conditionNames = conditions.Where(x=>x!=null).Select(x=> string.IsNullOrEmpty(x.objective) ? x.name.ToUpper() : x.objective);

            float popupWidth = 200;
            var propRect = new Rect(rect);
            var listRect = new Rect(rect);
            propRect.width -= popupWidth + 10;
            listRect.x += propRect.width + 10;
            listRect.width = popupWidth;

            //Draw the popup box with the current selected index
            EditorGUI.BeginProperty(rect, label, property);
            EditorGUI.PropertyField(propRect, property, label);

            var index = conditions.IndexOf(property.objectReferenceValue as ObjectiveNode);
            index = EditorGUI.Popup(listRect, "", index, conditionNames.ToArray() );
            if (index >= 0) property.objectReferenceValue = conditions[index];

            EditorGUI.EndProperty();
        } else {
            EditorGUI.PropertyField(rect, property, label);
        }
    }
}

[CustomPropertyDrawer(typeof(ActivateObjectiveAttribute))]
public class ActivateObjectiveNodeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        ObjectiveNodeAttribute a = attribute as ObjectiveNodeAttribute;

        if (property.propertyType == SerializedPropertyType.Integer)
        {
            Component c = property.serializedObject.targetObject as Component;
            var objectives = c.GetComponent<ObjectiveGraphController>().GetObjectiveNodes();
            var objectiveNames = objectives.ConvertAll(x=>x.objective);

            float popupWidth = 200;
            var propRect = new Rect(rect);
            var listRect = new Rect(rect);
            propRect.width -= popupWidth + 10;
            listRect.x += propRect.width + 10;
            listRect.width = popupWidth;

            //Draw the popup box with the current selected index
            EditorGUI.BeginProperty(rect, label, property);
            EditorGUI.PropertyField(propRect, property, label);

            property.intValue = EditorGUI.Popup(listRect, "", property.intValue, objectiveNames.ToArray() );

            EditorGUI.EndProperty();
        } else {
            EditorGUI.PropertyField(rect, property, label);
        }
    }
}

#endif

}
