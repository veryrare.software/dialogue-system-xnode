using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VeryRare.DialogueSystem
{
    public class DialogueUIController : MonoBehaviour
    {
        // public UnityEvent OnConversationDone;
        // public UnityEvent OnConversationOpen;
        // public UnityEvent<int> OnConversationProgress;
        public bool resetDialogueOnReopen;
        public float scrollSpeed;

        public Canvas canvas;
        public RectTransform parent;
        public TMPro.TextMeshProUGUI linePrevious;
        public Button progressButton;
    
        public TMPro.TextMeshProUGUI lineSelf;
        public TMPro.TextMeshProUGUI lineSelfChoice;
        public TMPro.TextMeshProUGUI lineNpc;

        public bool playOnStart;
        [Header("Runtime:")]
        public bool isNPC;
        public bool isOpen;
        public int selectedIndex;
        public Actor Player; // todo load from somewhere

        DialogueRunner runner;

        bool animating = false;
        void Start()
        {
            runner = GetComponent<DialogueRunner>();
            runner.OnConversationEnd.AddListener(OnCloseDialogue);
            runner.OnUpdateLines.AddListener(OnUpdateLines);
            if ( playOnStart ) runner.OnOpen();
            canvas.enabled = false;
        }

        public virtual void OnUpdateLines(DialogueEntry entry)
        {
            List<ConversationLine> lines = entry.lines;
            bool isNPC = entry.isNPC;
            float timeLimit = entry.timeLimit;
            float delay = entry.timeDelay;
            Actor actor = entry.actor;

            animating = true;
            if ( !isOpen ) OpenDialogue();
            // Debug.Log("DialogueUIController.OnUpdateLinesw");
            this.isNPC = isNPC;

            // if its self line and there are prev lines and current has choices, copy the last ones text into the new one
            if (!isNPC && lines.Count > 1 && parent.childCount > 0)
            {
                string prevLine = parent.GetChild(parent.childCount-1).GetComponent<TMPro.TextMeshProUGUI>().text;
                linePrevious.text = prevLine;
                linePrevious.gameObject.SetActive(true);
            }
            else 
                linePrevious.gameObject.SetActive(false);

            // destroy prev ones
            for(int i = parent.childCount-1; i>=0; i--)
                Destroy(parent.GetChild(i).gameObject);

            StartCoroutine(OnUpdateLinesDelayed(lines, isNPC, timeLimit, delay, actor));
        }

        IEnumerator OnUpdateLinesDelayed(List<ConversationLine> lines, bool isNPC, float timeLimit, float delay, Actor actor)
        {
            yield return new WaitForSeconds(delay);
            var visibleLinesCount = lines.Count(x=>x.IsCheck && !x.IsHidden);
            selectedIndex = -1;
            for(int i = 0; i < lines.Count; i++)
            {
                var l = lines[i];
                if ( (isNPC && !l.IsCheck) || l.IsHidden ) continue; // if npc line and not isEnabled skip it

                var lineText = Instantiate(isNPC ? lineNpc : (lines.Count == 1 ? lineSelf : lineSelfChoice), parent);
                lineText.gameObject.SetActive(true);
                if ( l.textAlign == TextAlign.LEFT)
                    lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Left;
                else if (l.textAlign == TextAlign.CENTER) 
                    lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Center;
                else if (l.textAlign == TextAlign.RIGHT) 
                    lineText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Right;
                // if is npc or only one line (no choice), just instantiate, otherwise also line number and add button callback
                //TODO  l message -> replace all <actorName> with actor name
                var message = l.message; 
                message = LineProcessingHelper.ReplaceAll(l.message, "<playerName>", Player?.actorName);
                message = LineProcessingHelper.ReplaceAll(l.message, "<actor>", actor?.actorName);
                // todo move to dialog runner
                foreach(var a in runner.actorBindings)
                    message = LineProcessingHelper.ReplaceAll(message, "<"+a.name+">", a.value?.name);
                
                // message += " <color=#FF0000>check:"+l.IsCheck+" hidden:"+l.IsHidden+"</color>";
                if (!isNPC && visibleLinesCount > 1) 
                {
                    LineProcessingHelper.ProcessLine((i+1)+". " + message, lineText,  scrollSpeed);
                    lineText.GetComponent<Button>().interactable = l.IsCheck;
                    lineText.gameObject.AddComponent<DialogueLineButton>().index = i;
                } else {
                    selectedIndex = i;
                    LineProcessingHelper.ProcessLine(message, lineText, scrollSpeed);
                    break;
                }
            }
            progressButton.gameObject.SetActive(isNPC || visibleLinesCount == 1);
            EventSystem.current.SetSelectedGameObject(null);
            animating = false;
            if ( timeLimit > 0.01f)
            {
                yield return new WaitForSeconds(timeLimit);
                Progress();
            }
        }

        public void OnClick(int idx)
        {
            if (animating) return;
            SelectLine(idx);
            Progress();
        }

        void Update()
        {
            if ( !isOpen) return;
            if (!isNPC)
            {
                if ( Input.GetKeyDown(KeyCode.Alpha1) && parent.childCount >= 1) SelectLine(0);
                if ( Input.GetKeyDown(KeyCode.Alpha2) && parent.childCount >= 2) SelectLine(1);
                if ( Input.GetKeyDown(KeyCode.Alpha3) && parent.childCount >= 3) SelectLine(2);
                if ( Input.GetKeyDown(KeyCode.Alpha4) && parent.childCount >= 4) SelectLine(3);
                if ( Input.GetKeyDown(KeyCode.Alpha5) && parent.childCount >= 5) SelectLine(4);
                if ( Input.GetKeyDown(KeyCode.Alpha6) && parent.childCount >= 6) SelectLine(5);
                if ( Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) ) SelectLine(selectedIndex - 1);
                if ( Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) ) SelectLine(selectedIndex + 1);
            }
            //Progress Press
            if ( Input.GetKeyDown(KeyCode.Space))
            {
                if (parent.childCount == 1 || (selectedIndex >= 0 && selectedIndex < parent.childCount)) Progress();
            }
        }

        void SelectLine(int index)
        {
            selectedIndex = index;
        }

        void Progress()
        {
            Debug.Log("Progress -> " + selectedIndex);
            // OnConversationProgress.Invoke(selectedIndex);
            runner.Progress(selectedIndex);
        }
        
        void OpenDialogue()
        {
            // Debug.Log("DialogueUIController.OpenDialogue");
            isOpen = true;
            // OnConversationOpen.Invoke();
            canvas.enabled = true;
        }

        void OnCloseDialogue()
        {
            isOpen = false;
            // OnConversationDone.Invoke();
            canvas.enabled = false;
            linePrevious.text = "";
        }

    }

}
