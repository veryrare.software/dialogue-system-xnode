using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VeryRare.DialogueSystem
{
    public class DialogueLineButton : MonoBehaviour
    {
        public int index;
        private Button b;

        void Awake()
        {
            b = GetComponent<Button>();
            b.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            GetComponentInParent<DialogueUIController>().OnClick(index);
            EventSystem.current.SetSelectedGameObject(null);
            b.Select();
        }
    }
}