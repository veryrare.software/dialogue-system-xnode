using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VeryRare.DialogueSystem
{
    public class DialogueEntry
    {
        public List<ConversationLine> lines;
        public bool isNPC;
        public float timeLimit;
        public float timeDelay;
        public Actor actor;
    }

    public class DialogueRunner : MonoBehaviour
    {
        public DialogueObject dialogueObject;
        DialogueObject cachedDialogueObject;
        public DialogueSelfNode overrideStartNode; 
        [SerializeReference] public List<AbstractPropertyBinder>    propertyBindings; 
        [SerializeReference] public List<TriggerBinder>             triggerBindings;
        [SerializeReference] public List<ActorBinder>               actorBindings;
        
        public UnityEvent OnConversationOpen;
        public UnityEvent<DialogueEntry> OnUpdateLines;
        public UnityEvent OnConversationEnd;

        DialogueSelfNode currentNode;

        public void OnOpen() 
        {
            if ( currentNode == null) ResetConversation();
            OnConversationOpen.Invoke();
            foreach(var p in propertyBindings) p.UpdatePropertyValue();
            OnProgressNext(currentNode);
        }

        public bool IsReset() => currentNode == dialogueObject.startNode;
        
        public void ResetConversation() 
        {
            currentNode = dialogueObject.startNode;
            foreach(var a in actorBindings) a.UpdatePropertyValue();
        }

        public void Progress(int idx)
        {
            if ( currentNode == null || currentNode.lines.Count <= idx || idx < 0 )
            {
                // Debug.Log("DialogueRunner.Progress => currentNode is null ");
                EndConversation();
                return;
            } 
            if ( currentNode is DialogueNPCNode)
            {
                foreach(var l in currentNode.lines)
                    if ( ProgressLine(l) ) return;
                return;
            }
            // 
            ProgressLine(currentNode.lines[idx]);
        }

        bool ProgressLine(ConversationLine l)
        {
            foreach(var p in propertyBindings) p.UpdatePropertyValue();
            if (!l.IsCheck) return false;
            l.wasVisited = true;

            foreach(var t in triggerBindings) t.UpdatePropertyValue();
            foreach(var t in l.GetTriggerNodes())
                t.GetProperty().Trigger(l);

            var nextNode = l.OutputNode;
            OnProgressNext(nextNode);
            return true;
        }

        void OnProgressNext(DialogueNode nextNode)
        {
            if ( nextNode != null)
            {
                if (nextNode is DialogueSelfNode dialogueNode)
                    currentNode = dialogueNode;
                else if (nextNode is LoopNode loopNode)
                    currentNode = loopNode.dialogueNode;

                InvokeUpdateLines();
            } else 
            {
                EndConversation();
            }
        }

        public void EndConversation()
        {
            currentNode = null;
            OnConversationEnd.Invoke();
        }

        public void UpdateLines()
        {
            foreach(var p in propertyBindings) p.UpdatePropertyValue();
            InvokeUpdateLines();
        }

        void InvokeUpdateLines()
        {
            // Debug.Log("InvokeUpdateLines");
            OnUpdateLines.Invoke(
                new DialogueEntry()
                {
                    lines = currentNode.lines, 
                    isNPC = currentNode is DialogueNPCNode, 
                    timeLimit = currentNode.timeLimit, 
                    timeDelay = currentNode.timeDelay, 
                    actor = currentNode is DialogueNPCNode npcNode ? npcNode.actor : null
                }
            );
        }

        public void SetBoolValueTrue(string name)       => SetBoolValue(name, true);
        public void SetBoolValueFalse(string name)       => SetBoolValue(name, false);
        public void SetBoolValue(string name, bool v)       => (propertyBindings.Find(x=>x.name == name) as BoolBinder).value = v;
        public void SetIntValue(string name, int v)         => (propertyBindings.Find(x=>x.name == name) as IntBinder).value = v;
        public void SetStringValue(string name, string v)   => (propertyBindings.Find(x=>x.name == name) as StringBinder).value = v;

        public bool GetBoolValue(string name)       => (propertyBindings.Find(x=>x.name == name) as BoolBinder).value;
        public int GetIntValue(string name)         => (propertyBindings.Find(x=>x.name == name) as IntBinder).value;
        public string GetStringValue(string name)   => (propertyBindings.Find(x=>x.name == name) as StringBinder).value;

        public void ResetVisited()
        {
            foreach(var n in dialogueObject.nodes) 
                if ( n is DialogueSelfNode d) 
                    foreach(var l in d.lines)
                        l.wasVisited = false;
        }

#if UNITY_EDITOR

        void OnValidate()
        {
            if ( dialogueObject != cachedDialogueObject)
            {
                SyncBinding();
                cachedDialogueObject = dialogueObject;
            }
        }

        // creates runner inspector properties
        [ContextMenu("SyncBinding")]
        public void SyncBinding()
        {
            if ( dialogueObject == null) return; 
            // sync properties in inspector... remove non-existing and expose new ones, if ther dont exist in current bindings 
            // first remove non-existing properties
            propertyBindings.RemoveAll(propBind=>propBind==null||!dialogueObject.properties.Contains(propBind.GetPropertySO()));
            // then add new properties, type 
            foreach(var prop in dialogueObject.properties)
            {
                int idx = propertyBindings.FindIndex(x=>x.GetPropertySO()==prop);
                if ( idx == -1)
                {
                    if ( prop is BoolProperty b)
                        propertyBindings.Add(new BoolBinder() { property = b, name = b.name, value = b.value } );
                    else if ( prop is IntProperty i)
                        propertyBindings.Add(new IntBinder() { property = i, name = i.name, value = i.value } );
                    else if ( prop is StringProperty s)
                        propertyBindings.Add(new StringBinder() { property = s, name = s.name, value = s.value } );
                }
            }

            // repeat for other properties - triggers and actors
            triggerBindings.RemoveAll(tb=>tb==null||!dialogueObject.properties.Contains(tb.GetPropertySO()));
            foreach(var prop in dialogueObject.properties)
            {
                if ( prop is not TriggerProperty ) continue;
                int idx = triggerBindings.FindIndex(x=>x.GetPropertySO()==prop);
                if (idx == -1)
                    triggerBindings.Add(new TriggerBinder(){ property = prop as TriggerProperty, name = prop.name });
            }

            actorBindings.RemoveAll(ab=>ab==null||!dialogueObject.properties.Contains(ab.GetPropertySO()));
            foreach(var prop in dialogueObject.properties)
            {
                if ( prop is not ActorProperty a) continue;
                int idx = actorBindings.FindIndex(x=>x.GetPropertySO()==prop);
                if (idx == -1)
                    actorBindings.Add(new ActorBinder(){ property = a, name = prop.name, value = a.value });
            }

        }          
#endif

    }
}
