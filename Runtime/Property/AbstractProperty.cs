using System;
using System.Collections.Generic;
using UnityEngine;

namespace VeryRare.DialogueSystem
{
    #if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(AbstractProperty), true)]
    public class AbstractPropertyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            target.name = EditorGUILayout.TextField(new GUIContent("name"), target.name);
            base.OnInspectorGUI();
        }
    }
    #endif

    [System.Serializable]
    public class AbstractProperty : ScriptableObject, ICustomSerializable
    {
        public virtual Dictionary<string, object> Serialize()
        {
            var dict = new Dictionary<string, object>();
            dict["name"] = name;
            dict["type"] = GetType();
            return dict;
        }

        public virtual void DeSerialize(Dictionary<string, object> dict)
        {

        }

    }
}