using System.Collections.Generic;

namespace VeryRare.DialogueSystem
{
    public interface ICustomSerializable
    {
        Dictionary<string, object> Serialize();
        void DeSerialize(Dictionary<string, object> dict);

    }
}