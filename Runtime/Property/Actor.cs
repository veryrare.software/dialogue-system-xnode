using UnityEngine;

namespace VeryRare.DialogueSystem
{
    [CreateAssetMenu(menuName = "Dialogue System/Actor")]
    public class Actor : ScriptableObject
    {
        public string actorName;
        public override string ToString() => actorName ;
    }
}