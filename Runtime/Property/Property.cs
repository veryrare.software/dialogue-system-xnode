using System;
using UnityEngine;

namespace VeryRare.DialogueSystem
{
    [System.Serializable] 
    public class Property<T> : AbstractProperty 
    { 
        public T value;
        
        public override string ToString() => value?.ToString();
    }
}