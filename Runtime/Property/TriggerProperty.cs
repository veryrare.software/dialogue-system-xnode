using System;
using UnityEngine.Events;
using static VeryRare.DialogueSystem.DialogueSelfNode;

namespace VeryRare.DialogueSystem
{
    [System.Serializable] 
    public class TriggerProperty : Property<UnityEvent>
    {
        public void Trigger(ConversationLine l)
        {
            value.Invoke();
        }

        public override string ToString() => "";

    }
}