using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using XNode;

namespace VeryRare.DialogueSystem
{
    [System.Serializable]
    public enum TextAlign { DEFAULT = 0, LEFT = 1, CENTER = 2, RIGHT = 3} 

    [System.Serializable]
    public class ConversationLine : ICustomSerializable
    {
        [TextArea]
        public string message;
        public string [] localizedMessages;
        public AudioClip clip; // todo also localize?
        public string mood;
        public string anim;
        public Animation animClip;
        public TextAlign textAlign;
        public bool isChoice;
        
        public AudioClip lipSyncAudioClip; // todo also localize?
        public TextAsset lipSyncData;
        public Animation lipSyncAnimClip;

        public int cameraIndex;
        public PlayableAsset timeline; // also localize?

        [SerializeField] DialogueSelfNode node;
        [SerializeField] internal string outputPortGuid;
        [SerializeField] internal string checkPortGuid;
        [SerializeField] internal string hiddenPortGuid;
        [SerializeField] internal string triggerPortGuid;

        // TODO
        internal bool wasVisited;

        public ConversationLine(DialogueSelfNode node, string outputPort, string checkPort, string triggerPort, string hiddenPort)
        {
            this.node = node;
            this.outputPortGuid = outputPort;
            this.checkPortGuid = checkPort;
            this.triggerPortGuid = triggerPort;
            this.hiddenPortGuid = hiddenPort;
        }

        public NodePort OutputPort => node.GetOutputPort(outputPortGuid);
        public NodePort CheckPort => node.GetInputPort(checkPortGuid);
        public NodePort HiddenPort => node.GetInputPort(hiddenPortGuid);
        public NodePort TriggerPort => node.GetOutputPort(triggerPortGuid);

        internal DialogueNode OutputNode => OutputPort.Connection?.node as DialogueNode;
        internal LogicNode CheckNode => CheckPort.Connection?.node as LogicNode;
        internal LogicNode HiddenNode => HiddenPort.Connection?.node as LogicNode;
        internal List<TriggerNode> GetTriggerNodes() 
        {
            List<TriggerNode> triggers = new List<TriggerNode>();
            foreach (var c in TriggerPort.GetConnections()) 
                triggers.Add(c.node as TriggerNode);
            return triggers;
        }

        public bool IsCheck => CheckNode == null ? true : CheckPort.GetInputValue<bool>();
        public bool IsHidden => HiddenNode == null ? false : HiddenPort.GetInputValue<bool>();

        public bool WasVisited => wasVisited;

        public Dictionary<string, object> Serialize()
        {
            var dict = new Dictionary<string, object>();
            dict["message"] = message; 
            dict["clip"] = clip; 
            dict["mood"] = mood; 
            dict["anim"] = anim; 
            dict["textAlign"] = textAlign.ToString(); 
            dict["checkNode"] =  CheckNode?.guid; 
            dict["hiddenNode"] = HiddenNode?.guid;
            dict["outputNode"] = OutputNode?.guid;
            dict["trigerNodes"] = GetTriggerNodes().ConvertAll(x=>x.guid);
            return dict;
        }

        public void DeSerialize(Dictionary<string, object> dict)
        {
            message = (string)dict["message"];
            clip = (AudioClip)dict["clip"];
            mood = (string)dict["mood"];
            anim = (string)dict["anim"];
            textAlign = Enum.Parse<TextAlign>(dict["textAlign"] as string);
        }
    }

}
