using UnityEngine;

namespace VeryRare.DialogueSystem
{

    public abstract class AbstractPropertyNode<T> : BaseNode, IPropertyNode<T>  where T : AbstractProperty
    {
        [SerializeField]
        protected T property;
        public T GetProperty() => property;
        public void SetProperty(T prop) => property = prop;
    }

}