
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeTint(0.15f, 0.15f, 0.15f)]
    [NodeWidth(440)]
    public class DialogueNPCNode : DialogueSelfNode
    {
        [Input(ShowBackingValue.Never, ConnectionType.Override, TypeConstraint.Inherited)]
        public Actor actor;

    }


}
