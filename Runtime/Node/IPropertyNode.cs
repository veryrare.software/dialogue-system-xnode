
namespace VeryRare.DialogueSystem
{
    public interface IPropertyNode<T> where T : AbstractProperty
    {
        T GetProperty();
        void SetProperty(T property);
    }
}
