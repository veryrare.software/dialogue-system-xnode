using System.Collections.Generic;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeTint(0.3f, 0.3f, 0.3f)]
    [NodeWidth(440)]
    public class DialogueSelfNode : DialogueNode
    {
        public float timeLimit = 0f;
        public float timeDelay = 0f;
        public List<ConversationLine> lines = new List<ConversationLine>();

    }


}
