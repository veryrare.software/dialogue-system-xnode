using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace VeryRare.DialogueSystem
{
    public abstract class BaseNode : XNode.Node, ICustomSerializable
    {
        public string guid;

        protected BaseNode()
        {
            guid = Guid.NewGuid().ToString();
        }

        public override object GetValue(NodePort port) => null;


        public virtual Dictionary<string, object> Serialize()
        {
            // reduce for just the important (position, name, connections, lines..)
            var dict = new Dictionary<string, object>();
            dict["positionX"]    = position.x;
            dict["positionY"]    = position.y;
            dict["name"]        = name;
            dict["guid"]        = guid;
            dict["type"]        = GetType().ToString();
            // todo serialize all ports and connections or serialize specific data in each node and then reconstruct?
            return dict;
        }

        public virtual void DeSerialize(Dictionary<string, object> dict)
        {
            name = dict["name"] as string;
            var position = new Vector2((float)dict["positionX"], (float)dict["positionY"]);
            if ( position.x != 0 || position.y != 0 )
                this.position = position;
            var guid = dict["guid"] as string;
            if ( !string.IsNullOrEmpty(guid) ) this.guid = guid;
        }

    }
}