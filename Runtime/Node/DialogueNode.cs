using XNode;

namespace VeryRare.DialogueSystem
{
    public abstract class DialogueNode : BaseNode
    {
        [Input(ShowBackingValue.Never, ConnectionType.Multiple, TypeConstraint.None)]
        public DialogueNode input;

        public override object GetValue(NodePort port) => this;
    }
}