using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.65f, 0.35f, 0.3f)]
    public class ActorNode : AbstractPropertyNode<ActorProperty>
    {
        [Output(ShowBackingValue.Never)]
        public Actor output;

        public override object GetValue(NodePort port) => property?.value;
    }
}