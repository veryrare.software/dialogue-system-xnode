using XNode;

namespace VeryRare.DialogueSystem
{

    [NodeWidth(200)]
    [NodeTint(0.5f, 0.1f, 0.5f)]
    public class LoopNode : DialogueNode
    {
        public DialogueSelfNode dialogueNode;
    }
}