using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class StringNode : AbstractPropertyLogicNode<StringProperty>
    {
        public string compareToString;

        public override object GetValue(NodePort port) 
        {
            return property != null && property.value == compareToString;
        }
    }
}