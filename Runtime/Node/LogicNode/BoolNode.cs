using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class BoolNode : AbstractPropertyLogicNode<BoolProperty>
    {     
        public override object GetValue(NodePort port) => property != null && property.value;

        // public override bool Evaluate(List<AbstractPropertyBinder> propertyBindings) 
        // {
        //     if ( property == null ) return false;
        //     var b = propertyBindings.Find(x=>x.GetPropertySO()==property) as BoolBinder;
        //     if ( b == null) return false;
        //     return b.value;
        // }
    }
}