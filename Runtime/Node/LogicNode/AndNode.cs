using System.Collections.Generic;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(100)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class AndNode : LogicNode
    {
        [Input(ShowBackingValue.Never)] public bool inputA;
        [Input(ShowBackingValue.Never)] public bool inputB;

        // public override bool Evaluate(List<AbstractPropertyBinder> p) => (inputA != null && inputA.Evaluate(p)) && (inputB != null && inputB.Evaluate(p));

        public override object GetValue(NodePort port) => GetInputValue<bool>("inputA") && GetInputValue<bool>("inputB");
    }
}