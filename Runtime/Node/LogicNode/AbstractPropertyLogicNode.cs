using UnityEngine;

namespace VeryRare.DialogueSystem
{
    public abstract class AbstractPropertyLogicNode<T> : LogicNode, IPropertyNode<T>  where T : AbstractProperty
    {
        [SerializeField]
        protected T property;
        public T GetProperty() => property;
        public void SetProperty(T prop) => property = prop;
    }
}