using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class LineVisitedNode : LogicNode
    {     
        public DialogueSelfNode dialogueNode;
        public int line;

        public override object GetValue(NodePort port) => dialogueNode != null && dialogueNode.lines[line].wasVisited;
    }
}