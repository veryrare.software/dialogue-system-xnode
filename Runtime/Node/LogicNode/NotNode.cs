using System.Collections.Generic;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(100)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class NotNode : LogicNode
    {
        [Input(ShowBackingValue.Never)] public bool input;

        public override object GetValue(NodePort port)=> !base.GetInputValue<bool>("input");

        // public override bool Evaluate(List<AbstractPropertyBinder> p) => input != null && !input.Evaluate(p);
    }
}