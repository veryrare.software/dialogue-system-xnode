using System.Collections.Generic;
using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.3f, 0.5f, 0.75f)]
    public class IntNode : AbstractPropertyLogicNode<IntProperty>
    {
        public enum Comparison {
            EQUALS,
            GREATER,
            GREATER_EQUAL,
            LOWER,
            LOWER_EQUAL,
            NOT_EQUAL,
        }

        public Comparison comparison;
        public int compareToInt;

        public override object GetValue(NodePort port) => property != null && GetResult(property.value);

        // public override bool Evaluate(List<AbstractPropertyBinder> p)
        // {
        //     if ( property == null ) return false;
        //     var b = p.Find(x=>x.GetPropertySO() == property) as IntBinder;
        //     if ( b == null) return false;
        //     switch(comparison)
        //     {
        //         case Comparison.EQUALS:         return b.value == compareToInt;
        //         case Comparison.GREATER:        return b.value > compareToInt;
        //         case Comparison.GREATER_EQUAL:  return b.value >= compareToInt;
        //         case Comparison.LOWER:          return b.value < compareToInt;
        //         case Comparison.LOWER_EQUAL:    return b.value <= compareToInt;
        //     }
        //     return false;
        // }

        bool GetResult(int v)
        {
            switch(comparison)
            {
                case Comparison.EQUALS:         return v == compareToInt;
                case Comparison.GREATER:        return v > compareToInt;
                case Comparison.GREATER_EQUAL:  return v >= compareToInt;
                case Comparison.LOWER:          return v < compareToInt;
                case Comparison.LOWER_EQUAL:    return v <= compareToInt;
                case Comparison.NOT_EQUAL:    return v != compareToInt;
            }
            return false;
        }
    }
}