using System.Collections.Generic;
using XNode;

namespace VeryRare.DialogueSystem
{
    public abstract class LogicNode : BaseNode
    {
        [Output(ShowBackingValue.Never)]
        public bool output;

        // public abstract bool Evaluate(List<AbstractPropertyBinder> propertyBindings);
        
        public bool Check() => (bool) GetValue(GetOutputPort("output"));
    }
}