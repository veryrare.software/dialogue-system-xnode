using XNode;

namespace VeryRare.DialogueSystem
{
    [NodeWidth(200)]
    [NodeTint(0.8f, 0.7f, 0.1f)]
    public class TriggerNode : AbstractPropertyNode<TriggerProperty>
    {
        [Input(ShowBackingValue.Never)]
        public TriggerNode input;

    }
}