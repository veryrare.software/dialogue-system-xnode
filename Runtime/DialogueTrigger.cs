using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VeryRare.DialogueSystem;

namespace VeryRare.DialogueSystem
{
    public class DialogueTrigger : MonoBehaviour
    {
        public DialogueRunner runner;

        public enum TriggerType{ ON_TRIGGER, ON_PRESS_E};

        public TriggerType type;
        private bool playerInCollider;

        void OnTriggerEnter(Collider c)
        {
            if ( !c.CompareTag("Player") ) return;
            if ( type == TriggerType.ON_TRIGGER )
                runner.OnOpen();
            else
                playerInCollider = true;
        }

        void OnTriggerExit(Collider c)
        {
            if ( !c.CompareTag("Player") ) return;
            if ( type == TriggerType.ON_TRIGGER )
                runner.EndConversation();
            else
                playerInCollider = true;
        }

        void Update()
        {
            if ( type == TriggerType.ON_PRESS_E && Input.GetKeyDown(KeyCode.E) && playerInCollider) runner.OnOpen();
        }

        void OnValidate()
        {
            if ( runner == null) runner = GetComponentInChildren<DialogueRunner>();
        }

    }
}