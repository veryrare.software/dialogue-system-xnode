using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NestableButtonAttribute : PropertyAttribute
{
    public string methodName;

    public NestableButtonAttribute(string methodName)
    {
        this.methodName = methodName;
    }
}
