using System;
using UnityEngine;
using UnityEngine.Events;

namespace VeryRare.DialogueSystem
{
    [Serializable] 
    public class AbstractPropertyBinder 
    { 
        [HideInInspector] public string name; 
        public virtual AbstractProperty GetPropertySO() => null;
        public virtual void reset() {}
        public virtual void UpdatePropertyValue() {}
        public override string ToString()=>name;
    }

    public class PropertBinder<T> : AbstractPropertyBinder
    {
        public Property<T> property;
        [NestableButton("reset")]
        public T value;
        public override AbstractProperty GetPropertySO() => property;
        public override void UpdatePropertyValue() => property.value = value;
        public override void reset() { Debug.Log("Reseting "+ name + " to " + property.value); value = property.value; }
    }    

    // [Serializable] 
    // public class BoolBinder : AbstractPropertyBinder         
    // {
    //     public BoolProperty property;
    //     public bool value;
    //     public override AbstractProperty GetPropertySO() => property;
    //     public override void UpdatePropertyValue() => property.value = value;
    //     public override void ResetValue() => value = property.value;
    // }
    // [Serializable] 
    // public class IntBinder : AbstractPropertyBinder         
    // {
    //     public IntProperty property;
    //     public int value;
    //     public override AbstractProperty GetPropertySO() => property;
    //     public override void UpdatePropertyValue() => property.value = value;
    //     public override void ResetValue() => value = property.value;
    // }
    // [Serializable] 
    // public class StringBinder :  AbstractPropertyBinder      
    // {
    //     public StringProperty property;
    //     public string value;
    //     public override AbstractProperty GetPropertySO() => property;
    //     public override void UpdatePropertyValue() => property.value = value;
    //     public override void ResetValue() => value = property.value;
    // }
    // [Serializable] 
    // public class TriggerBinder : AbstractPropertyBinder
    // {
    //     public TriggerProperty property;
    //     public UnityEvent value;
    //     public override AbstractProperty GetPropertySO() => property;
    //     public override void UpdatePropertyValue() => property.value = value;
    //     public override void ResetValue() => value = property.value;
    // }
    // [Serializable] 
    // public class ActorBinder :   AbstractPropertyBinder      
    // {
    //     public ActorProperty property;
    //     public Actor value;
    //     public override AbstractProperty GetPropertySO() => property;
    //     public override void UpdatePropertyValue() => property.value = value;
    //     public override void ResetValue() => value = property.value;
    // }
    
    [Serializable] public class BoolBinder : PropertBinder<bool> {}
    [Serializable] public class IntBinder : PropertBinder<int> {}
    [Serializable] public class StringBinder : PropertBinder<string> {}

    [Serializable] public class TriggerBinder : AbstractPropertyBinder {
        public TriggerProperty property;
        public UnityEvent value;
        public override AbstractProperty GetPropertySO() => property;
        public override void UpdatePropertyValue() => property.value = value;
    }
    [Serializable] public class ActorBinder : PropertBinder<Actor> {}     

}
    