using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using System.Linq;
using UnityEngine.Events;

namespace VeryRare.ObjectiveSystem
{

public class ObjectiveGraphController : SceneGraph<ObjectiveGraph>
{
    [ObjectiveNode]
    [SerializeField] 
    List<ObjectiveNode> objectiveNodes = new List<ObjectiveNode>();
    
    [ContextMenu("REFRESH")]
    void OnValidate()
    {
        if (Application.isPlaying) return;
        if ( graph == null) return;
        objectiveNodes.RemoveAll(x=>x==null);
        objectiveNodes = objectiveNodes.Distinct().ToList();
        foreach(var node in graph.nodes)
        {
            if (node is ObjectiveNode o)
            {
                if ( !objectiveNodes.Contains(o) )
                    objectiveNodes.Add(o);
            }
        }

    }

    public List<ObjectiveNode> GetObjectiveNodes() => objectiveNodes;
}

public class ObjectiveNodeAttribute : PropertyAttribute
{
}

public class ActivateObjectiveAttribute : PropertyAttribute
{
}


}
