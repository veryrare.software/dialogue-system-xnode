using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace  VeryRare.ObjectiveSystem {


public class ObjectiveUIController : MonoBehaviour
{
    public RectTransform entryParent;
    public ObjectiveUIEntry prefab;

    [System.Serializable]
    public class Entry
    {
        public ObjectiveNode objectiveNode;
        public ObjectiveUIEntry uiEntry;
        public bool activated;
        public bool completed;
    }
    [SerializeField]
    List<Entry> entries;

    ObjectiveGraphController controller;
    Mask mask;

    void Start()
    {
        controller = GetComponent<ObjectiveGraphController>();
        mask = entryParent.GetComponent<Mask>();
        prefab.gameObject.SetActive(false);
        foreach(var o in controller.GetObjectiveNodes())
            OnAddObjective(o);
    }

    void OnAddObjective(ObjectiveNode o)
    {
        var uiEntry = Instantiate(prefab.gameObject).GetComponent<ObjectiveUIEntry>();
        uiEntry.gameObject.SetActive(true);
        uiEntry.transform.SetParent(entryParent);
        uiEntry.text.text = o.objective;
        uiEntry.completed.localScale = new Vector3(0,1,1);
        entries.Add(new Entry(){ objectiveNode = o, uiEntry = uiEntry });
    }

    private void OnActivateObjective(ObjectiveNode o)
    {
    }

    void Update()
    {
        foreach(var e in entries)
        {
            if (e.completed) continue;
            if (!e.activated) 
                e.activated = e.objectiveNode.IsActivated();
            if (!e.activated) continue;

            var outOf = e.objectiveNode.GetOutOf();
            if ( outOf <= 0)
                e.uiEntry.progress.text = "";
            else
                e.uiEntry.progress.text = (int)(outOf * (e.objectiveNode.GetProgress()+0.01f)) + " / " + (int)outOf;
                
            if (e.objectiveNode.IsCompleted())
            {
                e.completed = true;
                StartCoroutine(CompleteEntry(e));
            }
        }
    }

    IEnumerator CompleteEntry(Entry e)
    {
        float t = 0f;
        float duration = 0.2f;
        while(t < duration)
        {
            if ( e.uiEntry == null) yield break;
            e.uiEntry.completed.localScale = new Vector3(t/duration,1,1);
            t += Time.unscaledDeltaTime;
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        if ( e.uiEntry == null) yield break;
        var rt = e.uiEntry.transform as RectTransform;
        float h = rt.sizeDelta.y;
        t = 0f;
        while(t < duration)
        {
            if ( rt == null) yield break;
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, h * (1f - t / duration));
            t += Time.unscaledDeltaTime;
            yield return null;
        }
        if ( rt == null) yield break;
        rt.SetAsLastSibling();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, h);
    }

    public void ToggleMask()
    {
        mask.enabled = !mask.enabled;
    }
}

}