using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  VeryRare.ObjectiveSystem {

public class ObjectiveUIEntry : MonoBehaviour
{
    public TMPro.TextMeshProUGUI text;
    public RectTransform completed;
    public TMPro.TextMeshProUGUI progress;
}

}