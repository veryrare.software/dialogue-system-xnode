﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


[NodeWidth(400)]
public class ObjectiveNode : ObjectiveGraphBaseNode 
{
	public   string objective;
	[Input]  [SerializeField] bool active;
	[Input]  [SerializeField] bool input;
	[Input]  [SerializeField] float progress;
	[Input]  [SerializeField] float outOf;
	[Output] [SerializeField] bool isCompleted;

	public bool IsActivated() => GetInputValue<bool>("active", active);
	public float GetProgress() => GetInputValue<float>("progress", progress);
	public float GetOutOf() => GetInputValue<float>("outOf", outOf);

	public bool IsCompleted() => GetInputValue<bool>("input", input);

	public override object GetValue(NodePort port) => IsCompleted();

	private void Reset() 
	{
	}

}

}