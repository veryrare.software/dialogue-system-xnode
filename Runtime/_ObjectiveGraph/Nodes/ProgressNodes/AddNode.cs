﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;


namespace  VeryRare.ObjectiveSystem {

public class AddNode : MathNode 
{
    [Input] public float inputA;
    [Input] public float inputB;
    [Output] public float output;
	
    public override object GetValue(NodePort port)
    {
        return GetInputValue<float>("inputA", inputA) + GetInputValue<float>("inputB", inputB);
    }
}

}