﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {

public class MultiplyNode : MathNode 
{
    [Input] public float inputA = 1f;
    [Input] public float inputB = 1f;
    [Output] public float output;
	
    public override object GetValue(NodePort port)
    {
        return GetInputValue<float>("inputA", inputA) * GetInputValue<float>("inputB", inputB);
    }
}

}