﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {

public class OneMinusNode : MathNode 
{
    [Input] public float input;
    [Output] public float output;
	
    public override object GetValue(NodePort port)
    {
		  return 1f - GetInputValue<float>("input", input);
    }
}


}