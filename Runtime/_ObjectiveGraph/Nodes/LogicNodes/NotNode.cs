﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


public class NotNode : LogicNode 
{

	  [Input] public bool input;
	
    public override object GetValue(NodePort port)
    {
		  return !GetInputValue<bool>("input", input);
    }
}

}