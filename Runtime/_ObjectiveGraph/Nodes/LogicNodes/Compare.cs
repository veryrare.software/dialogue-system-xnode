
namespace  VeryRare.ObjectiveSystem {


public enum Compare
{
    EQUAL,
    NOT_EQUAL,
    LESS,
    LESS_EQUAL,
    GREATER,
    GREATER_EQUAL,
}

}