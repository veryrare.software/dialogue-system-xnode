﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {

[NodeWidth(300)]
public class BoolNode : LogicNode 
{

	public enum BoolType { ALL, ANY};
	public BoolType boolType;

	public BoolCondition [] boolConditions; 

	[Output] public float progress;
	[Output] public float outOf;

    public override object GetValue(NodePort port)
    {
		if ( port.fieldName == "output")
		{
			if ( boolType == BoolType.ANY) return boolConditions.Any(x=>x.Invoke());
			return boolConditions.All(x=>x.Invoke());
		}
		else if ( port.fieldName == "progress")
		{
			if ( boolType == BoolType.ANY) return boolConditions.Any(x=>x.Invoke()) ? 1f : 0f;
			return boolConditions.Where(x=>x.Invoke()).Count() * 1f / boolConditions.Count();
		}
		if ( port.fieldName == "outOf" )
		{
			if ( boolType == BoolType.ANY) return 1f;
			return boolConditions.Count();
		}
		return null;
    }

}

}