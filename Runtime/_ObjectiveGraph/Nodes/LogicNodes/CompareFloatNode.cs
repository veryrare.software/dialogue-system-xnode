﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


[NodeWidth(300)]
public class CompareFloatNode : LogicNode 
{
	[Input] public float inputA;
	public Compare compare;
	[Input] public float inputB;

    public override object GetValue(NodePort port)
    {
		var val = GetInputValue<float>("inputA", inputA);
		var compareValue = GetInputValue<float>("inputB", inputB);

		switch(compare)
		{
			case Compare.EQUAL: return val == compareValue;
			case Compare.NOT_EQUAL: return val != compareValue;
			case Compare.LESS: return val < compareValue;
			case Compare.LESS_EQUAL: return val <= compareValue;
			case Compare.GREATER: return val > compareValue;
			case Compare.GREATER_EQUAL: return val >= compareValue;
		}
        return null;
    }
}


}