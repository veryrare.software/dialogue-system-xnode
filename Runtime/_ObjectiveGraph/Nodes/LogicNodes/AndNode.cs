﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


public class AndNode : LogicNode
{
	[Input] public bool inputA;
	[Input] public bool inputB;

    public override object GetValue(NodePort port)
    {
		return GetInputValue<bool>("inputA", inputA) && GetInputValue<bool>("inputB", inputB);
    }
}
}