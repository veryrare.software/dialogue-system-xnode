﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


[NodeWidth(300)]
public class IntNode : LogicNode 
{
	public IntCondition intCondition; 
	public Compare compare;
	public int compareValue;


	[Output] public float progress;
	[Output] public int outOf;

    public override object GetValue(NodePort port)
    {
		int val = intCondition.Invoke();
		if ( port.fieldName == "progress")
		{
			return val;
		}
		if ( port.fieldName == "outOf" )
		{
			return compareValue;
		}

		switch(compare)
		{
			case Compare.EQUAL: return val == compareValue;
			case Compare.NOT_EQUAL: return val != compareValue;
			case Compare.LESS: return val < compareValue;
			case Compare.LESS_EQUAL: return val <= compareValue;
			case Compare.GREATER: return val > compareValue;
			case Compare.GREATER_EQUAL: return val >= compareValue;
		}
		return null;
    }
}


}