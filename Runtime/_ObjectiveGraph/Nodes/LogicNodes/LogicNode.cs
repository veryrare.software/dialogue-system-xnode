﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


[NodeWidth(140)]
public abstract class LogicNode : ObjectiveGraphBaseNode 
{
	[Output] public bool output;

	public override abstract object GetValue(NodePort port);
}

}