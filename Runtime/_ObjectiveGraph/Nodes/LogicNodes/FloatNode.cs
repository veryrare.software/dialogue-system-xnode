﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace  VeryRare.ObjectiveSystem {


[NodeWidth(300)]
public class FloatNode : LogicNode 
{

	public FloatCondition floatCondition; 
	public Compare compare;
	public float compareValue;

	[Output] public float progress;
	[Output] public int outOf;

    public override object GetValue(NodePort port)
    {
		float val = floatCondition.Invoke();
		if ( port.fieldName == "progress")
		{
			return val;
		}
		if ( port.fieldName == "outOf" )
		{
			return (int) compareValue;
		}

		switch(compare)
		{
			case Compare.EQUAL: return val == compareValue;
			case Compare.NOT_EQUAL: return val != compareValue;
			case Compare.LESS: return val < compareValue;
			case Compare.LESS_EQUAL: return val <= compareValue;
			case Compare.GREATER: return val > compareValue;
			case Compare.GREATER_EQUAL: return val >= compareValue;
		}
		return null;
    }
}

}