using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VeryRare.DialogueSystem
{
    [CreateAssetMenu(menuName = "Dialogue System/Dialogue", fileName = "Dialogue")]
    public class DialogueObject : XNode.NodeGraph
    {
        public DialogueSelfNode startNode;

        public List<AbstractProperty> properties = new List<AbstractProperty>();

        public string [] langs;
        
        public string SerializeToJson()
        {
            var dict = new Dictionary<string, object>();
            dict["startNode"] = startNode.guid;
            dict["nodes"] = nodes.ConvertAll(x=>x != null ? (x as BaseNode).Serialize() : null);
            dict["properties"] = properties.ConvertAll(x=>x != null ? x.Serialize() : null);
            var json = Json.Serialize(dict);
            return json; 
        }

        public void DeserializeFromJson(string s)
        {
            var dict = Json.Deserialize(s) as Dictionary<string,object>;
            foreach(Dictionary<string, object> propertyObj in dict["properties"] as List<Dictionary<string, object>>)
            foreach(Dictionary<string, object> nodeObj in dict["nodes"] as List<Dictionary<string, object>>)
            {
                // create new node nodeJson["type"] and deserialize
                Type t = Type.GetType(nodeObj["type"] as string); // will it work?
                if ( t == null) continue;
                BaseNode n = AddNode(t) as BaseNode;
                if ( n == null ) continue;
                n.DeSerialize(nodeObj);
            }

            // connect all nodes based on guids
            startNode = nodes.Find(x=>(x as BaseNode).guid == dict["startNode"] as string) as DialogueSelfNode;
            foreach(var n in nodes)
            {
                // if is type connect create specific connections.. basically just for DialogueSelfNode and Logic Nodes
            }
        }

        public void FindStartNode()
        {
             foreach(var n in nodes)
            {
                if ( n is DialogueSelfNode dn && dn.GetInputValue<DialogueNode>("input") == null )
                {
                    startNode = dn;
                    break;
                }
            }
        }

        public T CreateProperty<T>(string newPropName) where T : AbstractProperty
        {
            if ( properties == null) properties = new List<AbstractProperty>();
            if (properties.Exists(x=>x.name==newPropName)) return null;
            var prop = ScriptableObject.CreateInstance<T>();
            prop.name = newPropName;
#if UNITY_EDITOR
            Undo.RecordObject(this, "Dialogue Object (CreateNewPropert)");
#endif
            properties.Add(prop);
            // set prop parent in assets and save assets
#if UNITY_EDITOR
            AssetDatabase.AddObjectToAsset(prop, this);
            Undo.RegisterCreatedObjectUndo(prop, "Dialogue Object (CreateNewPropert)");
            AssetDatabase.SaveAssets();
#endif
            return prop;
        }

        public void DeleteProperty(AbstractProperty property)
        {
        }
    }


}